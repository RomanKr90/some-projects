import json
from CelestialBody import CelestialBody
import math as m

import os
import numpy as np
import scipy
from scipy.integrate import odeint
from numpy import linalg as LA

#astronomical unit (meters)
aunit = float(149597870691)
#meter in km
km = float(1000)
#Scalefunctions for distance
def scaleDistance(x):
    y = [0,0,0]
    for i in range(0,3):
        y[i] =m.log1p(m.fabs(x[i]))
        if(x[i]< 0):
            y[i]*=-1
    return y 

def scaleDistance2(x):
    return x
#===========================================================================
# Receives a value in km
# transforms the value in meter and then in au and uses the scaling function
#===========================================================================     
def scalePlanetSize(radiuskm,nRoot):   
    radiusau = float( radiuskm*km )/aunit
    newRadius = m.pow(radiusau,1.0/nRoot)   
    return newRadius

def normalizePlanetSize(scaledPlanetRadius,norm):
    newRadiusNormalized = float(scaledPlanetRadius)/(norm *7)   
    return newRadiusNormalized

#Transforms original coordinates in scaled coordinates in openGl coordinate system.
def openGlCoordinates(origCoordinates):
    scalCoordinates = scaleDistance2(origCoordinates) 
    openGlCoordinates = [scalCoordinates[0],scalCoordinates[2],-scalCoordinates[1]]
    return openGlCoordinates

















