'''
Created on 22.04.2013

@author: Roman
updated 06.07.2013 Roman
updated 16.08.2013 Roman
'''

import SpaceShip

from AuxMet import normalize, makeCelBodiesList ,readPlanetsMass,computeRotation
from Odes import *
import time
import numpy as np
import math
from scipy import optimize
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import matplotlib.pyplot as plt
#Gravitational constant
G= 6.6738480* math.pow(10, -11)
#Mass of the sun in kg
smass = 1.9891 *math.pow(10, 30)
#Timefactor - compute the acceleration in other units, default sec
asind = 3600 * 3600 *24 *24 #acceleration per day
dins =  3600 *24  #day in seconds
#astronomical unit (meters)
aunit = float(149597870691)
#meter in km
km = float(1000)
#Earthradius
earthRad = 6378137
class Simulation(object):
    '''
    Simulation beinhaltet alle Attribute und Methoden die fuer die Visualisierung benoetigt werden. 
    Sie wird vom Gui instanziiert, so dass der User die Simulation, durch Belegung der Attribute, einstellen kann.
    '''
    #sim = Simulation.__init__(self)
    def __init__(self):
        '''
        Constructor
        '''
        
            
        
        self.celBodiesList = makeCelBodiesList("solarsystem.json")
        #Additional planets
        nplanetsUser = 0
        #Number of planets without the sun, standard Planets + user defined
        self.nplanets = len(self.celBodiesList) - 1  + nplanetsUser        
        #Initial speed of the spaceship
        Initspeed = [0.00928759 ,  0.00274875, 0.01]
        #Initial position of the spaceship, earths position + factor:
        Initposition = self.shipStartPosition()
        #Number of stages:
        nstages = 3
        #Consumption for each stage:
        consumption = [200,200,200]
        #Thrust for each stage in newton * day^2
        thrust = [15*asind,5*asind,20*asind, 0]
        #Initial mass of the spaceship and the fuelmass of stages:
        stagesMass = [100000,10000,10000,10000]
        #Emptyweight of the stages
        emptyWeight = [10000,10000,1000]
        #Initiate the spaceship
        self.spaceShip = SpaceShip.SpaceShip(nstages, stagesMass ,  Initposition    , Initspeed,  stagesMass )
        #Initiate the ODE values                                    
        self.motionOde = Motionode(self.nplanets, self.spaceShip.nstages, readPlanetsMass(self.celBodiesList) , thrust ,consumption, emptyWeight, 0)
        #the time at which the previous computation took place    
        self.currentTime = 0       
        #starting time
        self.startTime = 0
        #Guess of the user for the initial values
        self.userGuess = None
        #Optimal solution
        self.optSolution = [ [0,0,Initspeed[0],Initspeed[1],Initspeed[2] ] ,1]
        #Which optimization possibility
        self.optimizeVariant = 2           
        #Parameters for time-scaling:
        #The Timescale factor by which the difference between the current time and time of the previous iteration is multiplicated
        #timeScale = 1 means, that 1 second of simulation time is equal do 1 day, = 2 that one second is equal to 2 days etc.
        self.timeScale = 1
        #Rocket scale factor
        self.rocketScale = 0.5
        #Scale parameters for the planets:
        self.planetScaleFactor1 = 100
        #Scale parameters for the planets:
        self.planetScaleFactor2 = 5
        #Scale parameter for the distance
        self.distanceScaleFactor = 1
        
  
    #Calculates in a given period of time t is given as a vector with timepoints for which the positions shall be computed
    def refresh(self,t):
        #Beispieldaten, sollten von der GUI uebergegeben werden!
        
        newTime = self.currentTime + t
        #initial Conditions
        y_0 = self.readInitialValue()
    
        #convert in meter
        y_0[0:6*self.nplanets+6] *= aunit
               
        #New positions and velocities are computed
        y_res = self.motionOde.simRPM(self.currentTime,newTime, y_0 ) 
       
        #All planets position and velocities as the rockets one and the masses of the rocket
        #are updated
        self.writeInitialValue(y_res)
        self.currentTime = newTime
        return y_res
        
    def refreshRotation(self,t):           
        for i in range(0,self.nplanets+1):
            rotDif = computeRotation(t,self.celBodiesList[i].rotPeriod)
            self.celBodiesList[i].curRot += rotDif
            self.celBodiesList[i].curRot %= 360
            
    def readInitialValue(self):    
        #Initialize initial conditionsvector
        y0 = np.zeros(6 * self.nplanets + 6+ 1 + self.spaceShip.nstages)
      
        #read the Planetspositions and speeds
        for i in range (0,self.nplanets):
            # Current Positions
            y0[i*6:i*6 + 3] = self.celBodiesList[i+1].position
            # Current Velocities
            y0[i*6+3:i*6 + 6] = self.celBodiesList[i+1].speed
        #read the RocketPosition and Velocity
        
        y0[6*self.nplanets:6*self.nplanets + 3] = self.spaceShip.position
        y0[6*self.nplanets+3 :6*self.nplanets + 6] = self.spaceShip.speed
       
        #read the RocketMasses   
        rocind = 6 * self.nplanets +6
        
        y0[rocind  : rocind + 1 + self.spaceShip.nstages] = self.spaceShip.rocketMasses
          
        return y0
    
    def writeInitialValue(self,y):
        for i in range (0,self.nplanets):
            # Current Positions
            self.celBodiesList[i+1].position = y[i*6:i*6 + 3] 
            # Current Velocities
            self.celBodiesList[i+1].speed = y[i*6+3:i*6 + 6] 
        
        #read the RocketPosition and Velocity
        self.spaceShip.position = y[6*self.nplanets:6*self.nplanets + 3] 
        self.spaceShip.speed = y[6*self.nplanets+3 :6*self.nplanets + 6]        
        #read the RocketMasses   
        rocind = 6 * self.nplanets +6       
        self.spaceShip.rocketMasses = y[rocind  : rocind + 1 + self.spaceShip.nstages] 
      

    
    def scalePlanetSize(self,radius):   
        newRadius= (radius)**(1.0/self.planetScaleFactor1)      
        return newRadius/self.planetScaleFactor2
    
    #===========================================================================
    # Receives a value in km
    # transforms the value in meter and then in au and uses the scaling function
    #===========================================================================
    def glRadius(self,radiuskm):
        planRadiusUnscaled = float( radiuskm*km )/aunit
        planRadius = self.scalePlanetSize( planRadiusUnscaled) 
        return planRadius
    
    #Return the current position of the earth + factor
    def shipStartPosition(self):

        return array( self.celBodiesList[3].position )  + array( self.celBodiesList[3].position )/LA.norm( self.celBodiesList[3].position)*0.2 
    
    
    def setInitSpeed(self,v):
        self.spaceShip.speed = v
    
    
    #After each start conditions should be set
    def startAfter(self,t):
        self.spaceShip.prevSpeed = self.spaceShip.speed
        self.refresh(t)
        self.spaceShip.position = self.shipStartPosition()
        self.spaceShip.speed = self.spaceShip.prevSpeed
        #Set the number of used stages to initial condition
        self.motionOde.nstagesUsed = 0
        #Set the masses of the spaceship to the initial masses
        self.spaceShip.rocketMasses = self.spaceShip.startMasses
        
    @staticmethod
    def distanceToMars(  paraToOptimize ):
        sim = Simulation()
        #set the parameters which are to optimize       
        t_start,t_encounter = paraToOptimize[0],paraToOptimize[1]
        initSpeed = paraToOptimize[2:5]
        #starting time point
        sim.startAfter(t_start)
        #Initialspeed
        sim.setInitSpeed(initSpeed)
        sim.refresh(t_encounter)
        #positions in AU
        marspos = sim.celBodiesList[4].position
        rocpos = sim.spaceShip.position
        print("current distance in AU")
        print(LA.norm(marspos-rocpos))
        
        return LA.norm(marspos-rocpos)
    
    
    def optimizeTrajectory(self):
        
        #Initial speed
        initSpeed = LA.norm(self.spaceShip.speed)
        #Initial distance from Earth to Mars
        initDistance = LA.norm(self.celBodiesList[4].position-self.spaceShip.position)
        print("Initial speed of the rocket and distance to mars in AU:")
        print(initSpeed,initDistance)
        #approximate time needed for the encounter is t = s/v where s is the distance and v the speed
        timeNeeded = initDistance/initSpeed
        #Constraint for the starting direction of the rocket
        earthSpeed = self.celBodiesList[3].speed
        earthPosition = self.celBodiesList[3].position
        #normalized earthspeed
        normEarthSpeed = earthSpeed/LA.norm(earthSpeed)
        #Direction away from the earth
        directionAway = earthPosition/LA.norm(earthPosition)
        #perpendicular vector
        perpVector = np.cross(normEarthSpeed,directionAway)
        #Make initial guesses for the right direction
        normPerpVector = normalize(perpVector)
        #normalize the speed of the spaceship
        normSpeed = LA.norm(initSpeed)
        #Vector of guesses
        guess = []
        guess.append( normPerpVector*normSpeed + directionAway*normSpeed )
        guess.append(-normPerpVector*normSpeed + directionAway*normSpeed)
        guess.append( normEarthSpeed*normSpeed + directionAway*normSpeed)
        guess.append( -normEarthSpeed*normSpeed + directionAway*normSpeed )
        
        if(self.userGuess!= [0,0,0] and self.optimizeVariant == 1):
            initial_guess = np.array(self.userGuess)
            print(self.userGuess)
            constraints = [(0.1*initial_guess[0],3*initial_guess[0]), (initial_guess[1]/5,initial_guess[1] *5), (-0.01,0.01),\
                            (-0.01,0.01), (-0.01,0.01)]
            #Different variations of optimization
            sol = optimize.fmin_l_bfgs_b(Simulation.distanceToMars, initial_guess , approx_grad= True, bounds = constraints  , factr=10**12 )
            # sol = optimize.fmin_powell(Simulation.distanceToMars, v, xtol=0.1, ftol=2 ,  maxiter = 1, maxfun = 10)
            # sol = optimize.fmin_cg(sim.distanceToMars, v, fprime=None ) 
            self.optSolution[0] = sol[0]
            self.optSolution[1] = sol[1]        
            print("Optimal Initial Values, minimal reachable distance to mars:")      
            print( sol[0], sol[1] )
        elif(self.userGuess!= [0,0,0] and self.optimizeVariant == 2):   
            for i in range(0,4):
                initial_guess = np.array([0.1, timeNeeded, guess[i][0] ,guess[i][1] , guess[i][2] ])
                #Approximative constraints
                constraints = [(0.1,0.5), (timeNeeded,timeNeeded*5), (-0.01,0.01), (-0.01,0.01), (-0.01,0.01)]
                #Different variations of optimization
                sol = optimize.fmin_l_bfgs_b(Simulation.distanceToMars, initial_guess , approx_grad= True, bounds = constraints  , factr=10**14 )
                # sol = optimize.fmin_powell(Simulation.distanceToMars, v, xtol=0.1, ftol=2 ,  maxiter = 1, maxfun = 10)
                # sol = optimize.fmin_cg(sim.distanceToMars, v, fprime=None ) 
                if( sol[1] < self.optSolution[1]):
                    self.optSolution[0] = sol[0]
                    self.optSolution[1] = sol[1]
                    print("New optimal solution")
                    print(self.optSolution[0])
                    print("minimal reachable distance to mars:")
                    print(self.optSolution[1])
               
                
            sol = optimize.fmin_l_bfgs_b(Simulation.distanceToMars, self.optSolution[0] , approx_grad= True, bounds = constraints  , factr=10**12 )    
            if( sol[1] < self.optSolution[1]):
                self.optSolution[0] = sol[0]
                self.optSolution[1] = sol[1]
                print("New optimal solution")
                print(self.optSolution[0])
                print("minimal reachable distance to mars:")
                print(self.optSolution[1])
                             
        self.startTime = self.optSolution[0][0]
        self.setInitSpeed(self.optSolution[0][2:5])
        self.timeScale2 = self.optSolution[0][1]/100.0
     
     
     
     
   

                        
## TEST AUSGABEN ###
if __name__ == '__main__':      
    sim = Simulation()
    
    #===========================================================================
    # sim.startAfter(2)
    # sim.optimizeTrajectory()
    # 
    #===========================================================================
    
    
    

    prevTime= time.time()
    iterTime = 0
    y = []
     
    for i in range(0,100):
        iterTime = time.time()
        y.append(sim.refresh(1)  )
        print("Iteration time")
        print(time.time()-iterTime)
         
         
    print("Needed time")
    print(time.time()-prevTime)
     
         
    result = np.asmatrix(y)
    np.asarray(result[:,0][0])
   
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot(xs=np.asarray(result[:,0]), ys=np.asarray(result[:,1]), zs=np.asarray(result[:,2]), zdir='z', label='ys=0, zdir=z')
    ax.plot(xs=np.asarray(result[:,12]), ys=np.asarray(result[:,13]), zs=np.asarray(result[:,14]), zdir='z', label='ys=0, zdir=z')
    ax.plot(xs=np.asarray(result[:,18]), ys=np.asarray(result[:,19]), zs=np.asarray(result[:,20]), zdir='z', label='ys=0, zdir=z',color = 'r')
    ax.plot(xs=np.asarray(result[:,60]), ys=np.asarray(result[:,61]), zs=np.asarray(result[:,62]), zdir='z', label='ys=0, zdir=z',color = 'm')    
      
    plt.show()  
    