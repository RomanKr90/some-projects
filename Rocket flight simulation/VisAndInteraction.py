from Visualisation import Visualisation

from Simulation import *
#from twisted.python.runtime import seconds
from numpy.ma.core import *

from numpy.oldnumeric.random_array import random_integers
from fnmatch import translate
from AuxMet import *


from OpenGL.raw.GL import *
from numpy.ma.core import *
import sys
import random
from ScaleFunctions import *
from random import randrange, uniform
from DrawCelBodies import *
from DrawRocket import rocket
from Background import drawBackground
from decimal import *
class VisAndInteraction(Visualisation):
	#Mouse smooth factor, the lesser, the smoother and weaker is the reaction to the mouse movement
	smoothFactor = 0.01
	#Defines how far the user moves in FPS view
	speedFactor = 1
	#The proportion function which is n-root
	nRoot = 7
	#Number of the planet which is used in comparison to other planets
	normPlanet = 3
	def __init__(self, sim):
	
		self.sim = sim  
		#set the start glRadius
		self.refreshGlRadius()   
		
		
		#Needed for realistic time behavior
		self.prevTime = time.time()  
		#The simulated time between two iteration
		self.timeElapsed = 0
		
		#         self.sim.startAfter(self.sim.startTime)
		#         self.sim.spaceShip.position = self.sim.shipStartPosition()
		
		
		glutInit(sys.argv)
		
		
		#Display mode:   
		#  Double buffer 
		#  RGBA color (Alpha unterstuetzt) 
		#  Depth buffer
		glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH) 
		glutInitWindowSize( self.windowWidth,self.windowHeight) 
		glutInitWindowPosition(0, 0) # Oben Links
		glutCreateWindow("Mission to Mars: Simulation")
		#Don't show the mouse cursor
		glutSetCursor(GLUT_CURSOR_NONE)
		#Set the starting point of the cursor
		glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH)/2, glutGet(GLUT_WINDOW_HEIGHT)/2)
			
		# Die Funktion zur Darstellung an glut registrieren. Wird am Anfang aufgerufen, kann auch manuell aufgerufen werden    
		glutDisplayFunc(self.DrawGLScene)
		
		#   glutFullScreen()
		
		# Wenn nichts passiert, soll immer wieder neu gezeichnet werden:
		# Die Aufrufhaeufigkeit ist abhaengig von den dargestellten Frames per Second
		glutIdleFunc(self.DrawGLScene) 
		
		# Funktion die beim veraendern der Fenstergroesse aufgerufen werden soll an glut registrieren
		# Wird bei Vollbild nicht benutzt. 
		glutReshapeFunc(self.ReSizeGLScene)
		
		
		glutPassiveMotionFunc(self.mouseMovement)
		# Bei Tastendruck wird die uebergebene Funktion mit der Taste als Parameter aufgerufen
		glutKeyboardFunc(self.keyPressed)
		
		#Bei Tastendruck von nicht ASCII-Tasten wie F1-12, PGUP/DOWN, alle Pfeiltasten
		glutSpecialFunc(self.specialKeys)
		
		#Initialize some start properties
		self.InitGL()
		
		# Der Main Loop wird gestartet und immer wieder durchlaufen, und ruft abhaengig von 
		# Ereignissen (z. B. Tastendruck, oder Aufforderung zum neu Zeichnen) die oben an 
		# glut uebergebenen Funktionen auf.  
		glutMainLoop()
		
		
	def keyPressed(self, key, mousePosX, mousePosY):
	
		# Ends the visualisation
		if key == '\033': # Esc
			sys.exit()
		
		#Fine scaling first, then coarser   
		if key == '\170': # x
			if(self.sim.timeScale>=-2 and self.sim.timeScale<2):
				self.sim.timeScale += 0.5 
			elif(self.sim.timeScale>=-10 and self.sim.timeScale<10):
				self.sim.timeScale += 1
			elif(self.sim.timeScale>=-50 and self.sim.timeScale<50):  
				self.sim.timeScale += 2  
		if key == '\171': # y
			if(self.sim.timeScale>-2 and self.sim.timeScale<=2):
				self.sim.timeScale -= 0.5 
			elif(self.sim.timeScale>-10 and self.sim.timeScale<=10):
				self.sim.timeScale -= 1
			elif(self.sim.timeScale>-50 and self.sim.timeScale<=50):  
				self.sim.timeScale -= 2  
		if(self.modelView == 1):
			if key == 'a':	
				self.centerX -= 0.5
			
			if key == 'd':	
				self.centerX += 0.5   
				
			if key == 'r':	
				self.centerY += 0.5  
				
			if key == 'f':	
				self.centerY -= 0.5	  
			
			if key == 'w':	
				self.centerZ -= 0.5	   
			
			if key == 's':	
				self.centerZ += 0.5	
				
		elif(self.modelView == 2):		
			if ( key=='w'):
				self.camMovFront = 1
					
			if (key=='s'):
				self.camMovFront = 2
			
			if (key=='d'):		   
				self.camMovRight = 1
						
			if (key=='a'):		   
				self.camMovRight = 2
				
			if (key=='r'):		   
				self.camMovUp = 1  
				
			if (key=='f'):		   
				self.camMovUp = 2  
			#===================================================================
			# The float numbers have a very little error in them, so for 
			# comparison a little smaller numbers are taken
			#===================================================================
			if (key == 'q'):	
				if(self.speedFactor >0.5):
					self.speedFactor  -= 0.3
				elif(self.speedFactor >0.051):
					self.speedFactor  -= 0.05
				else:
					self.speedFactor = 0.05
				
			if (key == 'e'):				
				
				if(self.speedFactor>=4.5):
					self.speedFactor = 4.5
				elif(self.speedFactor > 0.49):
					self.speedFactor  += 0.3
				else:
					self.speedFactor  += 0.05
				
				
						
			#If space button is pressed the user moves faster
			#after pressing it again slower
			if key == '\040':	  
				if(self.shift == False):
					self.shift= True
					self.speedFactor *= 4
				else:
					self.shift = False
					self.speedFactor /= 4	
			
		#1-Taste fuer Kamerapositionsreset
		if key == '1':
			self.modelView = 1
			print('Modelview')
			self.rotateX=0.0
			self.rotateY=0.0
			self.rotateZ=0.0
			
			self.centerX = 0
			self.centerY = 0
			self.centerZ = - 8
			
		if key == '2':
			self.modelView = 2
			print('FPS View')
			self.rotateX=0.0
			self.rotateY=0.0
					
			self.camMovFront = 0
			self.camMovRight = 0
			self.camMovUp = 0
			
			self.camPos = [0,0,-8]
			
			#Mouse smooth factor
			self.smoothFactor = 0.01
			
			glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH)/2, glutGet(GLUT_WINDOW_HEIGHT)/2)
			#Check value for the mouse bug
			self.mouseWarp = True
			
	
		if key == '.':
			self.rotateZ += 2
			if(self.rotateZ > 360):
				self.rotateZ -= 360 
		
		if key == ',':
			self.rotateZ -= 2
			if(self.rotateZ < 0):
				self.rotateZ += 360
				
			
	def specialKeys(self, key,  x, y):
		if key==GLUT_KEY_LEFT:
			
			self.rotateY += 2
			if(self.rotateY > 360):
				self.rotateY -= 360
				
		
		if key == GLUT_KEY_RIGHT:
	
			self.rotateY -= 2
			if(self.rotateY < 0):
				self.rotateY += 360
			
			
		if key == GLUT_KEY_UP:

			self.nRoot -= 0.3
			if(self.nRoot <1 ):
				self.nRoot = 1  			
			self.refreshGlRadius() 	
		if key== GLUT_KEY_DOWN:
		
			self.nRoot += 0.3
			if(self.nRoot > 1000):
				self.nRoot = 1000  
			self.refreshGlRadius() 
	def mouseMovement(self,  x,  y):
		
		if (self.mouseWarp == False):
			diffx=(x- glutGet(GLUT_WINDOW_WIDTH)/2.0 )*self.smoothFactor #check the difference between the 
			# current x and the last x position	 
			
			diffy=(y- glutGet(GLUT_WINDOW_HEIGHT)/2.0)*self.smoothFactor #check the difference between the 
			#  Check if the cursor doesn't point at the center
			if( (x != glutGet(GLUT_WINDOW_WIDTH)/2) or (y != glutGet(GLUT_WINDOW_HEIGHT)/2)):
			
				glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH)/2, glutGet(GLUT_WINDOW_HEIGHT)/2)
				self.mouseWarp = True
			
			self.rotateX += diffy#set the xrot to xrot with the addition
			#   of the difference in the y position
			self.rotateY +=  diffx#set the xrot to yrot with the addition
			#  of the difference in the x position
			
			self.rotateX %= 360
			self.rotateY %= 360
		else:
			self.mouseWarp = False

if __name__ == '__main__':


	testsim = Simulation.Simulation()
	testvis = VisAndInteraction(testsim)    