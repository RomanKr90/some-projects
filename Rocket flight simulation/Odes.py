'''
Created on 01.07.2013 Roman
Updated on 02.07.2013 Roman
'''

import SpaceShip

import math 

from mpl_toolkits.mplot3d import Axes3D
from copy import deepcopy
import time
import numpy as np
import scipy
from scipy.integrate import ode
from scipy.integrate import odeint
from numpy import array
from numpy import linalg as LA
import matplotlib
import matplotlib.pyplot as plt
import time


#Gravitational constant
G= 6.6738480* math.pow(10, -11)
#Mass of the sun in kg
smass = 1.9891 *math.pow(10, 30)
#Timefactor - compute the acceleration in other units, default sec
asind = 3600 * 3600 *24 *24 #acceleration per day
dins =  3600 *24  #day in seconds
#astronomical unit (meters)
aunit = 149597870691
#meter in km
km = 1000
#Earthradius
earthRad = 6378137
#eps
eps = 10 **(-6)

second_per_day = 24 * 3600
#Number of the stages Used
global nstagesUsed
nstagesUsed = 0
#relative tolerance for the ode solver
rtol = 1.0e-4

#Newtons gravitational law of motion
def gravitational_force(mass1, pos1, pos2):
    r = pos1 - pos2
    return second_per_day**2 * G * mass1 * r / LA.norm(r)**3
#Ode for the computation of the trajectory of the planets and the rocket
#Time point to which to integrate
#y - Input vector, first come 6 components- position and speed for each planet, then the position and velocity, together it is the vector of 6*nplanets + 6 length
#to this vector are added the mass of the rocket and each of the stage, so the total length of the input vector is 6*nplanets + 6 + 1  + nstages 
#nplanets - Number of the planets without the sun
#nstages - The amount of stages-
#propForce propulsion vector of dimension nstages+1, begining with the thrust of the first stages. The last component is for the thrust without the stages and should be zero
#stagesConsum - Stages consumption vector of dimenstion nstages begining begining with the first stage in kg/day
#m - the mass of the planets dimension nplanets respectively
#mstagesEmpty - empty weight in kg of each stage, begining with the first stage. Vector has the dimension nstages     
def odemao(t, y, nplanets, nstages, m ,propForce, stagesConsum, mstagesEmpty):
  
   
    #6 Elements for each planet 6 for rocket 1 for mass 
    y_res = np.zeros(nplanets*6 + 6 + 1 + nstages)   
    #Computation of the trajectory of the planets
    for i in range(0,nplanets):
        
        
        pv = y[i*6:i*6 + 6]
        #Position of the planet vector
        planpos = pv[0:3]  
        #Newtonian law of gravitation, acceleration is meter per day^2
        
        F = gravitational_force( smass, array([0, 0, 0]), planpos)
        
        
        #Add this to take in account the gravitation forces of the planets on each other
        for j in range (0,nplanets):        
            if (j!=i):
                F += gravitational_force(m[j], y[j*6:j*6 + 3], planpos)
                
                
        p_res = np.empty([6])
        
        #previous velocities
        p_res[0:3] = pv[3:6]
        p_res[3:6] = F
        
        y_res[i*6:i*6 + 6] = p_res
    
    #Computation of the trajectory of the rocket        
    F_G = np.empty([3])       
    rp = np.empty([3])
    rv = np.empty([3])
    
    rp = array(y[6*nplanets:6*nplanets+3]) #rocketposition
    rv = array(y[6*nplanets+3:6*nplanets+6]) #rocketvelocity
    
    #Gravitation forces of the Planets on the rocket y[i*6:i*6 + 3] are the positions of the planets
    for i in range (0,nplanets):
        F_G += gravitational_force(m[i],y[i*6:i*6 + 3],rp) 
    #Gravitation force of the sun on the rocket
    F_G += gravitational_force(smass,[0,0,0],rp)    
    
    
    
    global nstagesUsed
    #Masses of the stages equations
    if (nstagesUsed< nstages):         
        #check if there is still fuel in the current stage
        if(y[6*nplanets+ 6 + 1 +nstagesUsed] <= 0):                     
            #proceed with using the new Stage                               
            y_res[6*nplanets+ 6] = -stagesConsum[nstagesUsed]
            y_res[6*nplanets+ 6 + 1 + nstagesUsed] = -stagesConsum[nstagesUsed]
            nstagesUsed += 1
        #Else proceed with using the stage
        elif(nstagesUsed<nstages ):    
            y_res[6*nplanets+ 6] = -stagesConsum[nstagesUsed]
            y_res[6*nplanets+ 6 + 1 + nstagesUsed] = -stagesConsum[nstagesUsed]
    
    #  opposite Direction of the thrust of the propulsion, in other words the acceleration direction of the rocket
    #  due to the stages of the rocket
    mrock = y[6*nplanets+ 6 ]
    #Direction at which the rocket is accelerated at the moment
    dir_prop = array(rv/LA.norm(rv) )        
    F_prop = dir_prop*propForce[nstagesUsed]/mrock 
    
    y_res[6*nplanets:6*nplanets+3] = rv
    #m*a = F_total = F_G + F_propulsion
    y_res[6*nplanets+3:6*nplanets+6] = F_G + F_prop
    
    return y_res
   


class Motionode(object):
    
   
    def __init__(self, nplanets, nstages , planetsMass, propForce, stagesConsum ,mstagesEmpty, nstagesUsed):
        self.nplanets = nplanets
        self.nstages = nstages
        self.planetsMass = planetsMass
        self.propForce = propForce
        self.stagesConsum = stagesConsum
        self.mstagesEmpty = mstagesEmpty
        self.nstagesUsed = nstagesUsed
       
          
    def simRPM(self,t_start,t_end,y):
        
        #If there is new starting point and self.nstagesUsed is 0 again global nstagesUsed should be it too
        if(self.nstagesUsed == 0):
            global nstagesUsed
            nstagesUsed = 0
            
           
          
        #Dormand-Prince integration method
        myode = ode(odemao).set_integrator('dopri5',rtol = rtol)
        
        #Set initial Values
        myode.set_initial_value(y, t_start).set_f_params(self.nplanets, self.nstages, self.planetsMass ,self.propForce, self.stagesConsum, self.mstagesEmpty )
       
        # We integrate till next time unit
        myode.integrate(t_end)
        t_start, y = myode.t, myode.y
        
        if(self.nstagesUsed < nstagesUsed):
            #Drop the current stage
            #Reduce the total mass and update the object nstagesUsed
            y[6*self.nplanets+ 6] -= self.mstagesEmpty[self.nstagesUsed]
            self.nstagesUsed = nstagesUsed
               
        # Solution is converted back in astronomical units  
        y[0: self.nplanets*6 + 6] /= aunit     
                                               
        return y
    
     
  

 
if __name__ == '__main__':      
    #Masses of the planets are read    
    m = 2               
   

 