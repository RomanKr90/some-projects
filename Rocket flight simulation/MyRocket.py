
import OpenGL

#Creates a red sphere within a window made by GLUT
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
#from OpenGL.raw.GLUT import glutSolidTeapot
#from OpenGL.raw.GL import glFlush
import time
from Simulation import *
#from twisted.python.runtime import seconds
from numpy.ma.core import *
import math as m
from numpy.oldnumeric.random_array import random_integers
from fnmatch import translate
from AuxMet import *

from ScaleFunctions import *
from OpenGL.raw.GL import *
from numpy.ma.core import *
import sys
import random
from DrawCelBodies import *
from Background import drawBackground

#astronomical unit (meters)
aunit = float(149597870691)
#meter in km
km = float(1000)

Breite=2
Hoehe=1
Tiefe=2
ind = 0



#/diffuse light color variables
dlr = 0.8
dlg = 0.8
dlb = 0.8
#/ambient light color variables
alr = 0.01
alg = 0.01
alb = 0.01

#/light position variables
lx = 3.0
ly = 2.0
lz = 1.0
lw = 0.0


#The coordinate system used in computation of the positions differs from one in OpenGL,
#The y axis of the original system is the z-axis in the opposite direction in OpenGl,
#so the (x,y,z) vector in original coordinate system has the (x,z,-y) coordinate in OpenGL!
class Visualisation(DrawCelBodies):
    ''' Klasse fuer die Visualisierung der Simulation '''
    
    # Klassenvariable (wird von allen Instanzen der Klasse geteilt)
    sim = None
    
    rotateY=0.0
    rotateX=0.0
    rotateZ=0.0
    #The movement of the spectator focal point in all directions 
    centerX = 0
    centerY = 0
    centerZ = - 3
    
    
    camMovFront = 0
    camMovRight = 0
    camMovUp = 0
    
    camPos = [0,0,-5]
    #The Size of the window:
    windowWidth = 1600
    windowHeight = 800
    #Mouse smooth factor
    smoothFactor = 0.01
    #Check value for the mouse bug
    mouseWarp = True
    #Running speed
    shift = False
    #1 - Modelview, 2 - First person view
    modelView = 1
    #speedfactor
    speedFactor = 0.2
    #Vertex precision
    nSlices = 25
    #Movespeed
    moveFactor = 0.5
    quad = gluNewQuadric()    # create a new quadric object
    def __init__(self, sim):
        '''
        Der Konstruktor erstellt ein neues OpenGl Fenster, 
        und registriert die Funktionen der Visualisierungsklasse an glut. 
        '''
        
        self.sim = sim     
       
        
        #Needed for realistic time behavior
        self.prevTime = time.time()  
        
        
#         self.sim.startAfter(self.sim.startTime)
#         self.sim.spaceShip.position = self.sim.shipStartPosition()
        
       
        glutInit(sys.argv)
        
        
        #Display mode:   
        #  Double buffer 
        #  RGBA color (Alpha unterstuetzt) 
        #  Depth buffer
        glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH) 
        glutInitWindowSize( self.windowWidth,self.windowHeight) 
        glutInitWindowPosition(0, 0) # Oben Links
        glutCreateWindow("Mission to Mars: Simulation")
        #Don't show the mouse cursor
        glutSetCursor(GLUT_CURSOR_NONE)
        #Set the starting point of the cursor
        glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH)/2, glutGet(GLUT_WINDOW_HEIGHT)/2)
        
    #    glLightfv(GL_LIGHT0,GL_AMBIENT,[1,1,1,4])
        
        # Die Funktion zur Darstellung an glut registrieren. Wird am Anfang aufgerufen, kann auch manuell aufgerufen werden    
        glutDisplayFunc(self.DrawGLScene)
        
        #   glutFullScreen()
        
        # Wenn nichts passiert, soll immer wieder neu gezeichnet werden:
        # Die Aufrufhaeufigkeit ist abhaengig von den dargestellten Frames per Second
        glutIdleFunc(self.DrawGLScene) 
        
        # Funktion die beim veraendern der Fenstergroesse aufgerufen werden soll an glut registrieren
        # Wird bei Vollbild nicht benutzt. 
        glutReshapeFunc(self.ReSizeGLScene)
        
        
        glutPassiveMotionFunc(self.mouseMovement)
        # Bei Tastendruck wird die uebergebene Funktion mit der Taste als Parameter aufgerufen
        glutKeyboardFunc(self.keyPressed)
        
        #Bei Tastendruck von nicht ASCII-Tasten wie F1-12, PGUP/DOWN, alle Pfeiltasten
        glutSpecialFunc(self.specialKeys)
        
        # Some start properties
        self.InitGL()
        
        
        # Der Main Loop wird gestartet und immer wieder durchlaufen, und ruft abhaengig von 
        # Ereignissen (z. B. Tastendruck, oder Aufforderung zum neu Zeichnen) die oben an 
        # glut uebergebenen Funktionen auf.  
        glutMainLoop()





    
        
        


    def ReSizeGLScene(self, Width, Height):
        ''' Funktion die beim Veraendern der Fenstergroesse aufgerufen wird '''
        
        if Height == 0:                       
            Height = 1
    
        glViewport(0, 0, Width, Height)        # Viewport zuruecksetzen und Perspektiventransformation
        glMatrixMode(GL_PROJECTION)
     #   glLoadIdentity()
        gluPerspective(45.0, float(Width)/float(Height), 0.1, 190.0)
        glMatrixMode(GL_MODELVIEW)
    #    glLoadIdentity()
    

    
    
    def DrawGLScene(self):
        '''
        Funktion die im OpenGl Mainloop aufgerufen wird um die Objekte zu zeichnen, 
        z.B. am Anfang, oder durch glutPostRedisplay()   
        '''

        #actual time - previous time
        curTime = time.time()
        timeElapsed = ( curTime - self.prevTime )*self.sim.timeScale
        self.prevTime = curTime
        #self.sim.refresh(   timeElapsed  )
                  
        #-------init------------
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glColor3f(1.0,1.0,1.0)#all white
        #Initial matrix, where all camera updates take place
        glPushMatrix()
        
   
        glPushMatrix()
        drawBackground()
        glPopMatrix()
        glPushMatrix()
        # particle()
        glPopMatrix()
        
        
        
        
        if(self.modelView == 1):
            self.modelView1()
        
        
        #The translations are performed, after that the system is rotated around the origin  
        elif(self.modelView == 2):
            self.modelView2()
               
      
                 
 

        #-------make sun--------
    
        #=======================================================================
        # glPushMatrix()
        # #rotationfunction of the sun
        #   
        # name = self.sim.celBodiesList[0].name         
        # plandiameterunscaled = float( self.sim.celBodiesList[0].radius*km * 2 )/aunit
        # plandiameter = self.sim.scalePlanetSize( plandiameterunscaled)    
        # Visualisation.celestialObject(name, plandiameter, "./images/"+self.sim.celBodiesList[0].texture)
        # glPopMatrix()
        #=======================================================================
        
        #Second sun
        glPushMatrix()
        #rotationfunction of the sun
       
        
        plandiameterunscaled = float( self.sim.celBodiesList[0].radius*km * 2 )/aunit
        plandiameter = self.sim.scalePlanetSize( plandiameterunscaled)    
        global ind
        ind += 1
        Visualisation.celestialObject("Affe",  "./images/"+self.sim.celBodiesList[3].texture, plandiameter,-25,ind)
        glPopMatrix()          
                  
        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE)
        
        glColor4f(1.0,0.3,0.2,0)
        gluSphere(self.quad, 0.2, 40, 40)
        
        glDisable(GL_BLEND)
        glColor4f(1,1,1,1)           
                  
              
        #self.drawPlanets() # erstellt alle Objekte des Sonnensystems
        #=======================================================================
        # glTranslatef(1,0.2,-1)
        # glRotatef(-45,1,0,0)
        #=======================================================================
        #glRotatef(45,0,0,1)
        #glRotatef(-345,0,1,0)
        glTranslatef(1,0,0)
        self.drawRocket() 
        
       
        
        #End of the initial matrix and of all updates at the current iteration 
        glPopMatrix()
        
            
        glutSwapBuffers() 
     
     
    def drawRocket(self):      
        rocpos = scaleDistance2(self.sim.spaceShip.position )
        rocspeed = scaleDistance2(self.sim.spaceShip.speed )
        
        v = [rocspeed[0],rocspeed[1],rocspeed[2] ]
        
        angles = computeAngle(v)
        
        glPushMatrix()


      
#         print("Direction X Y Z:")
#         print(rocspeed)
        
   
     #   glRotatef(angles[0],1.0,0,0)          
      #  glRotatef(angles[1],0,1.0,0)
     #   glRotatef(angles[2],0,0,1.0)  
        
        self.rocket(0.02 , 2, 25)
       
       
       
       
        glPopMatrix()     
       
        
            
    def rocket(self,size,nStages,nSlices):
        
       
        
        #Center of the rocket
        glPushMatrix()        
        
        #Scales all the stages
        glScale(size,size,size)
        
       # glMaterialfv(GL_FRONT,GL_EMISSION,[1.0,0.5,0.6])      
        
        DiffuseLight = [dlr, dlg, dlb]# //set DiffuseLight
        #[] to the specified values
        AmbientLight= [alr, alg, alb] #set AmbientLight to the specified values
        glLightfv (GL_LIGHT1, GL_DIFFUSE, DiffuseLight)# //change the light accordingly
        glLightfv (GL_LIGHT0, GL_AMBIENT, AmbientLight)# //changethe light accordingly
        LightPosition = [lx, ly, lz, lw] #//set the LightPosition to the specified values
        #Set the position of the diffused light of the sun to the center of the coordinate system
        glLightfv (GL_LIGHT1, GL_POSITION, [0,0,0,1])
        
        
        #glMaterialfv(GL_FRONT, GL_AMBIENT, [-1, 1, 0.25])
        #glMaterialfv(GL_FRONT, GL_DIFFUSE, [1, 0.40, 0.40])
        glMaterialfv(GL_FRONT, GL_SPECULAR, [0.37, 0.37, 0.37])
        glMaterialfv(GL_FRONT, GL_SHININESS, GLfloat(76.8))
        glColor3f(1,0,0)          
        self.drawFirstStage(nSlices)    
        glColor3f(0,1,0)  
        self.drawSecondStage(nSlices)
        glColor3f(0,0,1)             
        self.drawThirdStage(nSlices)
             
        self.drawSpaceCabin(nSlices)
    
        glPopMatrix()
      # glMaterialfv(GL_FRONT,GL_EMISSION,[0.7,0.7,0.8]) 
        
    def drawJets(self,nSlices):        
        gluDisk(self.quad,0,0.3,nSlices,1)
        gluCylinder(self.quad,0.3,0.1,0.3,nSlices,1)
        
        
        
        glPushMatrix()
        #Rotate all the jets to the right formation 
        glRotatef(45,0,0,1)
        #Engines
        glPushMatrix()
        
        glTranslatef(0,0,-0.1)
        gluDisk(self.quad,0,0.1,nSlices,1)
        gluCylinder(self.quad,0.1,0.06,0.1,nSlices,1)
        glPopMatrix()
        
        glPushMatrix()
      
        glTranslatef(0,0.2,-0.1)
        gluDisk(self.quad,0,0.1,nSlices,1)
        gluCylinder(self.quad,0.1,0.06,0.1,nSlices,1)
        glPopMatrix()
        
        glPushMatrix()
        
        glTranslatef(0,-0.2,-0.1)
        gluDisk(self.quad,0,0.1,nSlices,1)
        gluCylinder(self.quad,0.1,0.06,0.1,nSlices,1)
        glPopMatrix()
        
        glPushMatrix()
      
        glTranslatef(0.2,0,-0.1)
        gluDisk(self.quad,0,0.1,nSlices,1)
        gluCylinder(self.quad,0.1,0.06,0.1,nSlices,1)
        glPopMatrix()
        
        glPushMatrix()

        glTranslatef(-0.2,0,-0.1)
        gluDisk(self.quad,0,0.1,nSlices,1)
        gluCylinder(self.quad,0.1,0.06,0.1,nSlices,1)
        glPopMatrix()
        
        glPopMatrix()
    
    
    def drawAllEngines(self,nSlices):
        
        glPushMatrix()       
        #Right starting spot for drawing a cylinder
        glTranslatef(0,0,-0.3)
        #Middle Engine
        self.drawJets(nSlices)    
        
        #Upper and lower Engines
        glPushMatrix()       
        glTranslatef(0.5,0.5,0)
        self.drawJets(nSlices)    
        glPopMatrix()
        
        glPushMatrix()       
        glTranslatef(-0.5,0.5,0)
        self.drawJets(nSlices)    
        glPopMatrix()
        #left and right Engines
        glPushMatrix()       
        glTranslatef(0.5,-0.5,0)
        self.drawJets(nSlices)    
        glPopMatrix()
        
        glPushMatrix()       
        glTranslatef(-0.5,-0.5,0)
        self.drawJets(nSlices)    
        glPopMatrix()
        
        glPopMatrix()
        
    def drawFirstStage(self,nSlices):
              
        #Draw the jets of the first stage
        self.drawAllEngines(nSlices)
        #Draw the corpus of the first stage
        gluDisk(self.quad,0,1,nSlices,1)        
        gluCylinder(self.quad,1,1,5,nSlices,1)
        #Draw the three additional turbines
        self.drawTurbinesFirstStage(nSlices)
        
        
      
        glTranslatef(0,0,5)
       
        gluCylinder(self.quad,1,1,0.4,nSlices,1)
        glTranslatef(0,0,0.4)
      
    
    
    def drawTurbinesFirstStage(self,nSlices):
        
          
        #Steps of the first block,the upper one             
        self.firstBlockTurbines(nSlices)
        
        glPushMatrix()
           
        glRotatef(90,0,0,1)
        self.firstBlockTurbines(nSlices)
      
        glPopMatrix()
               
        #Steps of the first block,left one
        glPushMatrix()
              
        glRotatef(-90,0,0,1)
        self.firstBlockTurbines(nSlices)
        
        glPopMatrix()
    
        
    
    def firstBlockTurbines(self,nSlices):
        glPushMatrix()
        
        glTranslatef(0,1.5,0)
        
        
        
        
        
        glPushMatrix()
      
        
        glTranslatef(0,-0.75,0)
        
        glBegin(GL_QUADS)
        
        glVertex3f(-0.2, 0, 0)   
        glVertex3f(0.2, 0,0 )     
        glVertex3f(0.2, 0.4, 0)
        glVertex3f(-0.2, 0.4, 0)
        
        glEnd()
        
        glTranslatef(-0.2,0,0)
        
        glBegin(GL_QUADS)
        
        glVertex3f(0, 0, 0)   
        glVertex3f(0, 0, 4.2)    
        glVertex3f(0, 0.4, 4.2) 
        glVertex3f(0, 0.4, 0)
        
        glEnd()
        
        
        
        glTranslatef(0.4,0,0)
        
        glBegin(GL_QUADS)
        
        glVertex3f(0, 0, 0)   
        glVertex3f(0, 0, 4.2)    
        glVertex3f(0, 0.4, 4.2)  
        glVertex3f(0, 0.4, 0)
        
        glEnd()
                   
        glPopMatrix()
        
        
        #Draw the jets of the additional turbines
        glPushMatrix()       
        glTranslatef(0.3,0,-0.3)
        self.drawJets(nSlices) 
        glPopMatrix()
        
        glPushMatrix()       
        glTranslatef(-0.3,0,-0.3)
        self.drawJets(nSlices) 
        glPopMatrix()
        
        gluDisk(self.quad,0,0.5,nSlices,1) 
        gluCylinder(self.quad,0.5,0.4,2.2,nSlices,1)             
        glTranslatef(0,0,2.2)
        gluDisk(self.quad,0,0.4,nSlices,1)
              
        glTranslatef(0,0,-0.1)
        
        glRotatef(9,1,0,0)
        gluCylinder(self.quad,0.4,0.3,1.5,nSlices,1)      
        glTranslatef(0,0,1.4)
        
        glRotatef(9,1,0,0)
        gluCylinder(self.quad,0.3,0.15,1.4,nSlices,1) 
                            
        glPopMatrix()
    
        
    def drawSecondStage(self,nSlices):   
        #Second stage
        #Create Engine of the second stage
        glPushMatrix()       
        glTranslatef(0,0,-0.3)
        self.drawJets(nSlices)    
        glPopMatrix()
        #Upper and lower jets
        glPushMatrix()       
        glTranslatef(0,0.7,-0.3)
        self.drawJets(nSlices)    
        glPopMatrix()
        
        glPushMatrix()       
        glTranslatef(0,-0.7,-0.3)
        self.drawJets(nSlices)    
        glPopMatrix()
        #left and right jets
        glPushMatrix()       
        glTranslatef(0.7,0,-0.3)
        self.drawJets(nSlices)    
        glPopMatrix()
        
        glPushMatrix()       
        glTranslatef(-0.7,0,-0.3)
        self.drawJets(nSlices)    
        glPopMatrix()
        
        #End of engine of second stage
        #Second stage

        gluDisk(self.quad,0,1,nSlices,1)
        gluCylinder(self.quad,1,1,3,nSlices,1)
        
        glTranslatef(0,0,3)
        #draw second transition
    
        gluCylinder(self.quad,1,0.7,0.4,nSlices,1)
        glTranslatef(0,0,0.4)
    
        
            
        
    def drawThirdStage(self,nSlices):   
        #Draw one jet for the last one
        glPushMatrix()       
        glTranslatef(0,0,-0.3)       
        self.drawJets(nSlices)
        glPopMatrix()
       
        
      
        gluDisk(self.quad,0,0.7,nSlices,1)
        gluCylinder(self.quad,0.7,0.4,3,nSlices,1)
        glTranslatef(0,0,3)
        
        gluCylinder(self.quad,0.4,0.3,0.4,nSlices,1)
        glTranslatef(0,0,0.4)
 
    
    def drawSpaceCabin(self,nSlices): 
        
        glPushMatrix()       
        glTranslatef(0,0,-0.3)       
        self.drawJets(nSlices)
        glPopMatrix()
           
 
        gluDisk(self.quad,0,0.3,nSlices,1)
        gluCylinder(self.quad,0.3,0.3,0.55,nSlices,1)
        glTranslatef(0,0,0.55)
        
          
        gluDisk(self.quad,0,0.3,nSlices,1)    
        gluCylinder(self.quad,0.3,0.065,0.25,nSlices,1)
        glTranslatef(0,0,0.25)
      
        gluDisk(self.quad,0,0.065,nSlices,1)

    
       
    
    
        
    def modelView1(self):    
        #-------Update the viewpoint of the user--------
        #Translate the whole solar system with the sun as a center around (centerX,centerY,centerZ) aunit away from the user
        glTranslatef( self.centerX, self.centerY ,self.centerZ  )   
          
        #Update the viewangle  
        glRotatef(self.rotateX, 1.0,0.0,0.0)
        glRotatef(self.rotateY, 0.0,1.0,0.0)       
        glRotatef(self.rotateZ, 0,0.0,1.0)
    
    def modelView2(self):
        
        #-------Update the viewpoint of the user--------
        #Rotation of the whole solar system with the sun as a center, which simulates the point of view of the user        
        
        glRotatef(self.rotateX,1.0,0.0,0.0)
        glRotatef(self.rotateY,0.0,1.0,0.0)
        
        #If the user wants to move from the first person view
        #The GL_MODELVIEW_MATRIX is loaded which contains three direction vectors.
        #These vectors are the x,y,z axes in the coordinate system after the rotation with the values 
        #of these three vectors in the original coordinate system
        #In other words, it gives the current up,right and back view direction of a spectator
        #It seems that GL_MODELVIEW_MATRIX uses another coordinate system, the z axis goes in the opposite direction,
        #which is why 2 components at the translation have another sign than the last one
        if(self.camMovFront != 0):
            
            camMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)           
            if(self.camMovFront== 1):
                self.camPos += normalize(camMatrix[2][0:3] )*self.speedFactor
            else: 
                self.camPos -= normalize(camMatrix[2][0:3] )*self.speedFactor      
            self.camMovFront = 0
            
        elif(self.camMovRight != 0):
            
            camMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)           
            if(self.camMovRight== 1):
                self.camPos += normalize(camMatrix[0][0:3] )*self.speedFactor
            else: 
                self.camPos -= normalize(camMatrix[0][0:3] )*self.speedFactor      
            self.camMovRight = 0
            
        elif(self.camMovUp != 0):
            
            camMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)           
            if(self.camMovUp== 1):
                self.camPos += normalize(camMatrix[1][0:3] )*self.speedFactor
            else: 
                self.camPos -= normalize(camMatrix[1][0:3] )*self.speedFactor      
            self.camMovUp = 0
       
        v = self.camPos       
        glTranslatef(-v[0],-v[1],v[2])      
    
    
    
    
    def keyPressed(self, key, mousePosX, mousePosY):
        '''
        Funktion die von OpenGl aufgerufen wird, wenn eine Taste gedrueckt wurde.
        Esc: beenden
        x: Zeitverlauf beschleunigen
        y: Zeitverlauf verlangsamen
        '''
        # TODO: Kamera wechseln Taste
        if key == '\033': # Esc
            sys.exit()
        
        #Fine scaling first, then coarser   
        if key == '\170': # x
            if(self.sim.timeScale>=-2 and self.sim.timeScale<2):
                self.sim.timeScale += 0.5 
            elif(self.sim.timeScale>=-10 and self.sim.timeScale<10):
                self.sim.timeScale += 1
            elif(self.sim.timeScale>=-50 and self.sim.timeScale<50):  
                self.sim.timeScale += 2  
        if key == '\171': # y
            if(self.sim.timeScale>-2 and self.sim.timeScale<=2):
                self.sim.timeScale -= 0.5 
            elif(self.sim.timeScale>-10 and self.sim.timeScale<=10):
                self.sim.timeScale -= 1
            elif(self.sim.timeScale>-50 and self.sim.timeScale<=50):  
                self.sim.timeScale -= 2  
        if(self.modelView == 1):
            if key == 'a':    
                self.centerX -= 0.5*self.moveFactor
            
            if key == 'd':    
                self.centerX += 0.5*self.moveFactor   
                 
            if key == 'r':    
                self.centerY += 0.5*self.moveFactor  
                
            if key == 'f':    
                self.centerY -= 0.5*self.moveFactor      
            
            if key == 'w':    
                self.centerZ -= 0.5*self.moveFactor       
            
            if key == 's':    
                self.centerZ += 0.5*self.moveFactor    
                
        elif(self.modelView == 2):        
            if ( key=='w'):
                self.camMovFront = 1
                       
            if (key=='s'):
                self.camMovFront = 2
            
            if (key=='d'):           
                self.camMovRight = 1
                        
            if (key=='a'):           
                self.camMovRight = 2
                
            if (key=='r'):           
                self.camMovUp = 1  
                
            if (key=='f'):           
                self.camMovUp = 2  
            #If space button is pressed the user moves faster
            #after pressing it again slower
            if key == '\040':      
                if(self.shift == False):
                    self.shift= True
                    self.speedFactor *= 4
                else:
                    self.shift = False
                    self.speedFactor /= 4    
              
        #1-Taste fuer Kamerapositionsreset
        if key == '1':
            self.modelView = 1
            print('Modelview')
            self.rotateX=0.0
            self.rotateY=0.0
            self.rotateZ=0.0
            
            self.centerX = 0
            self.centerY = 0
            self.centerZ = - 10
            
        if key == '2':
            self.modelView = 2
            print('FPS View')
            self.rotateX=0.0
            self.rotateY=0.0
                       
            self.camMovFront = 0
            self.camMovRight = 0
            self.camMovUp = 0
            
            self.camPos = [0,0,-10]
            
            #Mouse smooth factor
            self.smoothFactor = 0.01
            
            glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH)/2, glutGet(GLUT_WINDOW_HEIGHT)/2)
            #Check value for the mouse bug
            self.mouseWarp = True
            
         
           
       
        if key == '.':
            self.rotateZ += 2
            if(self.rotateZ > 360):
                self.rotateZ -= 360 
        
        if key == ',':
            self.rotateZ -= 2
            if(self.rotateZ < 0):
                self.rotateZ += 360
                
            
    def specialKeys(self, key,  x, y):
        if key==GLUT_KEY_LEFT:
            
            self.rotateY += 2
            if(self.rotateY > 360):
                self.rotateY -= 360
            
           
        
        if key == GLUT_KEY_RIGHT:
           
            self.rotateY -= 2
            if(self.rotateY < 0):
                self.rotateY += 360
            
           
            
        if key == GLUT_KEY_UP:
   
            self.rotateX += 2
            if(self.rotateX > 360):
                self.rotateX -= 360

           
            
        if key== GLUT_KEY_DOWN:
            
            self.rotateX -= 2
            if(self.rotateX < 0):
                self.rotateX += 360  
               
           
           
                
    def mouseMovement(self,  x,  y):
        
        if (self.mouseWarp == False):
            diffx=(x- glutGet(GLUT_WINDOW_WIDTH)/2.0 )*self.smoothFactor #check the difference between the 
            # current x and the last x position     
            #print(diffx)
            diffy=(y- glutGet(GLUT_WINDOW_HEIGHT)/2.0)*self.smoothFactor #check the difference between the 
            #  Check if the cursor doesn't point at the center
            if( (x != glutGet(GLUT_WINDOW_WIDTH)/2) or (y != glutGet(GLUT_WINDOW_HEIGHT)/2)):
            
                glutWarpPointer(glutGet(GLUT_WINDOW_WIDTH)/2, glutGet(GLUT_WINDOW_HEIGHT)/2)
                self.mouseWarp = True
            
            self.rotateX += diffy#set the xrot to xrot with the addition
            #   of the difference in the y position
            self.rotateY +=  diffx#set the xrot to yrot with the addition
            #  of the difference in the x position
        else:
            self.mouseWarp = False
            


if __name__ == '__main__':
  
    
    testsim = Simulation.Simulation()
    
    testvis = Visualisation(testsim)        
        