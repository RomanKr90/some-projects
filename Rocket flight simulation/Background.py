
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys


from Simulation import *
#from twisted.python.runtime import seconds
from numpy.ma.core import *
import math as m


from OpenGL.raw.GL import *
from numpy.ma.core import *
import sys
import random
from DrawCelBodies import *






def backgroundPic():
    glPushMatrix()
    
    glEnable(GL_COLOR_MATERIAL)
    texturePic="./images/Milchstrasse_1.jpg"
    from PIL import Image as coimage
    Texture = coimage.open(texturePic)
    ix = Texture.size[0]
    iy = Texture.size[1]
    Texture = Texture.tostring("raw", "RGBX", 0, -1)
    glPixelStorei(GL_UNPACK_ALIGNMENT,1)
    glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 1, GL_RGBA, GL_UNSIGNED_BYTE, Texture)
    
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
    name = gluNewQuadric()
    gluQuadricNormals(name, GLU_SMOOTH)
    gluQuadricOrientation(name, GLU_OUTSIDE)
    glEnable(GL_TEXTURE_2D)
    gluQuadricTexture(name, GL_TRUE)
    
    Breite=2
    Hoehe=1
    Tiefe=2
    glMaterialfv(GL_FRONT,GL_EMISSION,[0.7,0.7,0.7])
    glBegin(GL_QUADS)
    glTexCoord2f(0,0)
    glVertex3f(-Breite, -Hoehe, -Tiefe)
    glTexCoord2f(1,0); 
    glVertex3f(+Breite, -Hoehe, -Tiefe);
    glTexCoord2f(1,1); 
    glVertex3f(+Breite, +Hoehe, -Tiefe);
    glTexCoord2f(0,1); 
    glVertex3f(-Breite, +Hoehe, -Tiefe);
    glEnd()
    glDisable(GL_TEXTURE_2D); 
    gluDeleteQuadric(name)
    glPopMatrix()
    glMaterialfv(GL_FRONT,GL_EMISSION,[0,0,0])
    

def drawBackground():
    glDisable(GL_DEPTH_TEST)
    glMatrixMode(GL_MODELVIEW)   #
    glLoadIdentity()     #
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);#
    glMatrixMode(GL_PROJECTION)
    glPushMatrix()
    backgroundPic()
    glPopMatrix()
    glClear(GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST)
    glClear(GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()   #