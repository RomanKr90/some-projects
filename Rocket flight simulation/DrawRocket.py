
import OpenGL

#Creates a red sphere within a window made by GLUT
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *


import time

#from twisted.python.runtime import seconds
from numpy.ma.core import *
import math as m
from numpy.oldnumeric.random_array import random_integers
from fnmatch import translate
from AuxMet import *

from OpenGL.raw.GL import *
from numpy.ma.core import *
import sys
import random
from DrawCelBodies import *
from Background import drawBackground


def rocket(quadric,size,nStages,nSlices):
          
    
    glPushMatrix()        
        
    #Scales all the stages
    glScale(size,size,size)
           
    #===========================================================================
    # Checks the number of stages,if the stage doesn't exist translates the same length in the z direction for the smooth
    # stage usage simulation
    #===========================================================================
    #Basic color: Silver-grey
    glColor3ub(65,65,65)
    if(nStages>=3):        
        drawFirstStage(quadric,nSlices) 
     
    if(nStages>=2):
        drawSecondStage(quadric,nSlices)
   
    if(nStages>=1):          
        drawThirdStage(quadric,nSlices)

    drawSpaceCabin(quadric,nSlices)

    glPopMatrix()
    
#===========================================================================
# Draws one engine with jets
#===========================================================================
def drawJets(quadric,nSlices):    
    #Save the previous color
    curColor = glGetFloatv(GL_CURRENT_COLOR) 

    glColor3ub(45,0,0)
    #Draw the engine   
    gluDisk(quadric,0,0.3,nSlices,1)
    gluCylinder(quadric,0.3,0.1,0.3,nSlices,1)
     
    glColor3ub(13,13,13)   
    glPushMatrix()
    #Rotate all the jets to the right formation 
    glRotatef(45,0,0,1)
    #Draw the jets on the engine
    glPushMatrix()
    glTranslatef(0,0,-0.1)
    gluDisk(quadric,0,0.1,nSlices,1)
    gluCylinder(quadric,0.1,0.06,0.1,nSlices,1)
    glPopMatrix()
    
    glPushMatrix()  
    glTranslatef(0,0.2,-0.1)
    gluDisk(quadric,0,0.1,nSlices,1)
    gluCylinder(quadric,0.1,0.06,0.1,nSlices,1)
    glPopMatrix()
    
    glPushMatrix()    
    glTranslatef(0,-0.2,-0.1)
    gluDisk(quadric,0,0.1,nSlices,1)
    gluCylinder(quadric,0.1,0.06,0.1,nSlices,1)
    glPopMatrix()
    
    glPushMatrix() 
    glTranslatef(0.2,0,-0.1)
    gluDisk(quadric,0,0.1,nSlices,1)
    gluCylinder(quadric,0.1,0.06,0.1,nSlices,1)
    glPopMatrix()
    
    glPushMatrix()
    glTranslatef(-0.2,0,-0.1)
    gluDisk(quadric,0,0.1,nSlices,1)
    gluCylinder(quadric,0.1,0.06,0.1,nSlices,1)
    glPopMatrix()
    
    glPopMatrix()
    #Function which takes a float color vector
    glColor4fv(curColor)
#===============================================================================
# Function which draws the jets for the first and second stages
#===============================================================================
def drawAllEngines(quadric,nSlices):
   
    glPushMatrix()       
    #Right starting spot for drawing jets
    glTranslatef(0,0,-0.3)
    
    #Middle Engine
    drawJets(quadric,nSlices)     
    #Upper and lower Engines
    glPushMatrix()       
    glTranslatef(0.5,0.5,0)
    drawJets(quadric,nSlices)    
    glPopMatrix()
    
    glPushMatrix()       
    glTranslatef(-0.5,0.5,0)
    drawJets(quadric,nSlices)    
    glPopMatrix()
    #left and right Engines
    glPushMatrix()       
    glTranslatef(0.5,-0.5,0)
    drawJets(quadric,nSlices)    
    glPopMatrix()
    
    glPushMatrix()       
    glTranslatef(-0.5,-0.5,0)
    drawJets(quadric,nSlices)    
    glPopMatrix()
    
    glPopMatrix()
    
def drawFirstStage(quadric,nSlices):
    #Save the previous color
    curColor = glGetFloatv(GL_CURRENT_COLOR) 
       
    #Draw the jets of the first stage
    drawAllEngines(quadric,nSlices)
    #Draw the three additional turbines
    drawTurbinesFirstStage(quadric,nSlices) 
    #Draw the corpus of the first stage
    gluDisk(quadric,0,1,nSlices,1)        
    gluCylinder(quadric,1,1,5,nSlices,1)     
    glTranslatef(0,0,5)
    #===========================================================================
    # Draw the transition to the next stage
    #===========================================================================
    glColor3ub(28,28,28)
    gluCylinder(quadric,1,1,0.4,nSlices,1)
    glTranslatef(0,0,0.4)
    
    glColor4fv(curColor)


def drawTurbinesFirstStage(quadric,nSlices):
    
    #Save the previous color
    curColor = glGetFloatv(GL_CURRENT_COLOR) 
  
    #Steps of the first block,the upper one             
    firstBlockTurbines(quadric,nSlices)
    
    glPushMatrix()
       
    glRotatef(90,0,0,1)
    firstBlockTurbines(quadric,nSlices)
  
    glPopMatrix()
           
    #Steps of the first block,left one
    glPushMatrix()
          
    glRotatef(-90,0,0,1)
    firstBlockTurbines(quadric,nSlices)
    
    glPopMatrix()

    glColor4fv(curColor)

def firstBlockTurbines(quadric,nSlices):
    #Pop at the end of the function
    glPushMatrix()
    
    glTranslatef(0,1.5,0)
    
    glPushMatrix()
  
    
    glTranslatef(0,-0.75,0)
    
    glBegin(GL_QUADS)
    
    glVertex3f(-0.2, 0, 0)   
    glVertex3f(0.2, 0,0 )     
    glVertex3f(0.2, 0.4, 0)
    glVertex3f(-0.2, 0.4, 0)
    
    glEnd()
    
    glTranslatef(-0.2,0,0)
    
    glBegin(GL_QUADS)
    
    glVertex3f(0, 0, 0)   
    glVertex3f(0, 0, 4.2)    
    glVertex3f(0, 0.4, 4.2) 
    glVertex3f(0, 0.4, 0)
    
    glEnd()
        
    glTranslatef(0.4,0,0)
    
    glBegin(GL_QUADS)
    
    glVertex3f(0, 0, 0)   
    glVertex3f(0, 0, 4.2)    
    glVertex3f(0, 0.4, 4.2)  
    glVertex3f(0, 0.4, 0)
    
    glEnd()
               
    glPopMatrix()
       
    #Draw the jets of the additional turbines
    glPushMatrix()       
    glTranslatef(0.3,0,-0.3)
    drawJets(quadric,nSlices) 
    glPopMatrix()
    
    glPushMatrix()       
    glTranslatef(-0.3,0,-0.3)
    drawJets(quadric,nSlices) 
    glPopMatrix()
    
    gluDisk(quadric,0,0.5,nSlices,1) 
    gluCylinder(quadric,0.5,0.4,2.2,nSlices,1)             
    glTranslatef(0,0,2.2)
    gluDisk(quadric,0,0.4,nSlices,1)
          
    glTranslatef(0,0,-0.1)
    
    glRotatef(9,1,0,0)
    gluCylinder(quadric,0.4,0.3,1.5,nSlices,1)      
    glTranslatef(0,0,1.4)
    
    glRotatef(9,1,0,0)
    gluCylinder(quadric,0.3,0.15,1.4,nSlices,1) 
                        
    glPopMatrix()

    
def drawSecondStage(quadric,nSlices):   
    #Save the previous color
    curColor = glGetFloatv(GL_CURRENT_COLOR)
    #Create the engines of the second stage
    drawAllEngines(quadric,nSlices)
    
    #End of engines of second stage

    gluDisk(quadric,0,1,nSlices,1)
    gluCylinder(quadric,1,1,3,nSlices,1)
    
    glTranslatef(0,0,3)
    #draw second transition
    glColor3ub(28,28,28)
    gluCylinder(quadric,1,0.7,0.4,nSlices,1)
    glTranslatef(0,0,0.4)
    
    #restore previous color
    glColor4fv(curColor)
        
    
def drawThirdStage(quadric,nSlices):  
    #Save the previous color
    curColor = glGetFloatv(GL_CURRENT_COLOR) 
    #Draw one jet for the last one
    glPushMatrix()       
    glTranslatef(0,0,-0.3)       
    drawJets(quadric,nSlices)
    glPopMatrix()
  
  
    gluDisk(quadric,0,0.7,nSlices,1)
    gluCylinder(quadric,0.7,0.4,3,nSlices,1)
    glTranslatef(0,0,3)
    #===========================================================================
    # Draw third transition
    #===========================================================================
    glColor3ub(28,28,28)
    gluCylinder(quadric,0.4,0.3,0.4,nSlices,1)
    glTranslatef(0,0,0.4)

    #restore previous color
    glColor4fv(curColor)

def drawSpaceCabin(quadric,nSlices): 
    
    glPushMatrix()       
    glTranslatef(0,0,-0.3)       
    drawJets(quadric,nSlices)
    glPopMatrix()
       

    gluDisk(quadric,0,0.3,nSlices,1)
    gluCylinder(quadric,0.3,0.3,0.55,nSlices,1)
    glTranslatef(0,0,0.55)
    
      
    gluDisk(quadric,0,0.3,nSlices,1)    
    gluCylinder(quadric,0.3,0.065,0.25,nSlices,1)
    glTranslatef(0,0,0.25)
  
    gluDisk(quadric,0,0.065,nSlices,1)