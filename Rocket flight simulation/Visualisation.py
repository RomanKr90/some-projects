



#from OpenGL.raw.GL import glFlush
import time
from Simulation import *
#from twisted.python.runtime import seconds
from numpy.ma.core import *

from numpy.oldnumeric.random_array import random_integers
from fnmatch import translate
from AuxMet import *


from OpenGL.raw.GL import *
from numpy.ma.core import *
import sys
import random
from ScaleFunctions import *
from random import randrange, uniform
from DrawCelBodies import *
from DrawRocket import rocket
from Background import drawBackground

#astronomical unit (meters)
aunit = float(149597870691)
#meter in km
km = float(1000)

ind = 0

#The coordinate system used in computation of the positions differs from one in OpenGL,
#The y axis of the original system is the z-axis in the opposite direction in OpenGl,
#so the (x,y,z) vector in original coordinate system has the (x,z,-y) coordinate in OpenGL!
class Visualisation(DrawCelBodies):
    ''' Klasse fuer die Visualisierung der Simulation '''
    
    # Klassenvariable (wird von allen Instanzen der Klasse geteilt)
    sim = None
    
    rotateY=0.0
    rotateX=0.0
    rotateZ=0.0
    #The movement of the spectator focal point in all directions 
    centerX = 0
    centerY = 0
    centerZ = -8
    
    
    camMovFront = 0
    camMovRight = 0
    camMovUp = 0
    
    camPos = [0,0,-8]
    #The Size of the window:
    windowWidth = 1600
    windowHeight = 800
    #Mouse smooth factor
    smoothFactor = 0.01
    #Check value for the mouse bug
    mouseWarp = True
    #Running speed
    shift = False
    #1 - Modelview, 2 - First person view
    modelView = 1
    #speedfactor
    speedFactor = 1
    #Vertex precision
    nSlices = 25
    #Movespeed
    moveFactor = 0.5
    #The proportion function which is n-root
    nRoot = 7
    #Number of the planet which is used in comparison to other planets
    normPlanet = 3
    
    quad = gluNewQuadric()    # create a new quadric object
    
    def ReSizeGLScene(self, Width, Height):
        ''' Funktion die beim Veraendern der Fenstergroesse aufgerufen wird '''
        
        if Height == 0:                       
            Height = 1
    
        glViewport(0, 0, Width, Height)        # Viewport zuruecksetzen und Perspektiventransformation
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45.0, float(Width)/float(Height), 0.1, 190.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
    
    
    def DrawGLScene(self):
        '''
        Funktion die im OpenGl Mainloop aufgerufen wird um die Objekte zu zeichnen, 
        z.B. am Anfang, oder durch glutPostRedisplay()   
        '''

        #actual time - previous time
        curTime = time.time()
        self.timeElapsed = ( curTime - self.prevTime )*self.sim.timeScale
        
        #Iteration time     
        print("Iteration time")
        print(curTime-self.prevTime)
        
        self.prevTime = curTime
        #Checking if timeElapsed and timeinterval for the ode is too small
        #Minimum is 1/1000 of the day
        if(self.timeElapsed<0.001):
            self.timeElapsed = 0.001
        #Update the data of the ode    
        self.sim.refresh(   self.timeElapsed  )
        #Update the rotation positions of the planets     
        self.sim.refreshRotation(self.timeElapsed )     
              
        #-------init------------
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glColor3f(1.0,1.0,1.0)#all white
        #Initial matrix, where all camera updates take place
        glPushMatrix()
                   
        if(self.modelView == 1):
            self.modelView1()
               
        #The translations are performed, after that the system is rotated around the origin  
        elif(self.modelView == 2):
            self.modelView2()
               
        #Rotate the whole system to the right starting point, so the viewer sees the earth and the rocket
        glRotatef(180,0,1.0,0)             
        #---------------------draw background, disable the lighting for the background and the sun------------------------------------------ 
        glDisable(GL_LIGHTING)
        glPushMatrix()
        drawBackground()
        glPopMatrix()
        #-------make sun--------      
        self.drawSun()
        
        glEnable(GL_LIGHTING)                    
        self.drawPlanets() 
        
        self.drawRocket() 
        
        #self.drawSunHalo()
        #End of the initial matrix and of all updates at the current iteration 
        glPopMatrix()
                   
        glutSwapBuffers() 
        
    def refreshGlRadius(self):    
        normRadScaled = scalePlanetSize(self.sim.celBodiesList[self.normPlanet].radius,self.nRoot)
        for i in range(0,self.sim.nplanets+1):
            radiusScaled = scalePlanetSize(self.sim.celBodiesList[i].radius,self.nRoot)
            self.sim.celBodiesList[i].glRadius = normalizePlanetSize(radiusScaled,normRadScaled)
            
    def drawSun(self):
        glPushMatrix()
        #rotationfunction of the sun
           
        name = self.sim.celBodiesList[0].name         
        
        #The sun is made bright
        glMaterialfv(GL_FRONT,GL_EMISSION,[0.7,0.7,0.5]) 
        self.celestialObject(name,  "./images/"+self.sim.celBodiesList[0].texture, self.sim.celBodiesList[0].glRadius,- self.sim.celBodiesList[0].tilt,
                                          self.sim.celBodiesList[0].curRot)
        glPopMatrix()   
        glMaterialfv(GL_FRONT,GL_EMISSION,[0,0,0])
        self.drawSunHalo(self.sim.celBodiesList[0].glRadius)
        
    def drawSunHalo(self,glRadius):    
        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE)
        
        glColor4f(1.0,0.3,0.2,0)
        
        haloRadius = glRadius + LA.norm(scaleDistance([0.03,0,0]) )
        gluSphere(self.quad, haloRadius, 70, 70)
        
        glDisable(GL_BLEND)
        glColor4f(1,1,1,1) 
        
    def drawPlanets(self):  
        
        for i in range(0,self.sim.nplanets):
            glPlanPos = openGlCoordinates(self.sim.celBodiesList[i+1].position)
            #Light direction from the sun to the planet
            glLightfv (GL_LIGHT1, GL_POSITION, -array([ glPlanPos[0],glPlanPos[1] , glPlanPos[2], 0]) )
            
            glPushMatrix()
            glTranslatef( glPlanPos[0],glPlanPos[1],glPlanPos[2])
                       
            name = self.sim.celBodiesList[i+1].name
      
            Visualisation.celestialObject(name, "./images/"+self.sim.celBodiesList[i+1].texture, self.sim.celBodiesList[i+1].glRadius, 
                                          - self.sim.celBodiesList[i+1].tilt,
                                          self.sim.celBodiesList[i+1].curRot)
            glPopMatrix()    
    
    def drawRocket(self):
        glRocPos = openGlCoordinates(self.sim.spaceShip.position )
        glRocSpeed = openGlCoordinates(self.sim.spaceShip.speed )
        
        #Computes the rotation angles of the rocket model    
        angles = computeAngle(glRocSpeed)
        #set the proper lighting for the rocket
        DiffuseLight1 = [0.3, 0.3, 0.3]
        glLightfv (GL_LIGHT1, GL_DIFFUSE, DiffuseLight1)
        glLightfv (GL_LIGHT1, GL_POSITION,-array([glRocPos[0] ,glRocPos[1],glRocPos[2],0]) )
        glEnable(GL_LIGHT0)
        glLightfv (GL_LIGHT0, GL_POSITION,array([glRocPos[0] ,glRocPos[1],glRocPos[2],0]) )
                   
        glPushMatrix()            
        glTranslatef(glRocPos[0] ,glRocPos[1],glRocPos[2]) 
              
        glRotatef(angles[1],0,1.0,0)    
        
        glRotatef(angles[0],1.0,0,0)   
   
        rocket(self.quad, 0.02 , 3 - self.sim.motionOde.nstagesUsed, 25)
       
        glPopMatrix()   
        
        DiffuseLight1 = [1, 1, 1]
        glLightfv (GL_LIGHT1, GL_DIFFUSE, DiffuseLight1)
        glDisable(GL_LIGHT0)
        
    def modelView1(self):    
        #-------Update the viewpoint of the user--------
        #Translate the whole solar system with the sun as a center around (centerX,centerY,centerZ) aunit away from the user
        glTranslatef( self.centerX, self.centerY ,self.centerZ  )   
          
        #Update the viewangle  
        glRotatef(self.rotateX, 1.0,0.0,0.0)
        glRotatef(self.rotateY, 0.0,1.0,0.0)       
        glRotatef(self.rotateZ, 0,0.0,1.0)
    
    def modelView2(self):
        
        #-------Update the viewpoint of the user--------
        #Rotation of the whole solar system with the sun as a center, which simulates the point of view of the user            
        glRotatef(self.rotateX,1.0,0.0,0.0)
        glRotatef(self.rotateY,0.0,1.0,0.0)       
        #If the user wants to move from the first person view
        #The GL_MODELVIEW_MATRIX is loaded which contains three direction vectors.
        #These vectors are the x,y,z axes in the coordinate system after the rotation with the values 
        #of these three vectors in the original coordinate system
        #In other words, it gives the current up,right and back view direction of a spectator
        #It seems that GL_MODELVIEW_MATRIX uses another coordinate system, the z axis goes in the opposite direction,
        #which is why 2 components at the translation have another sign than the last one
        if(self.camMovFront != 0):
            
            camMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)           
            if(self.camMovFront== 1):
                self.camPos += normalize(camMatrix[2][0:3] )*self.speedFactor
            else: 
                self.camPos -= normalize(camMatrix[2][0:3] )*self.speedFactor      
            self.camMovFront = 0
            
        elif(self.camMovRight != 0):
            
            camMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)           
            if(self.camMovRight== 1):
                self.camPos += normalize(camMatrix[0][0:3] )*self.speedFactor
            else: 
                self.camPos -= normalize(camMatrix[0][0:3] )*self.speedFactor      
            self.camMovRight = 0
            
        elif(self.camMovUp != 0):
            
            camMatrix = glGetFloatv(GL_MODELVIEW_MATRIX)           
            if(self.camMovUp== 1):
                self.camPos += normalize(camMatrix[1][0:3] )*self.speedFactor
            else: 
                self.camPos -= normalize(camMatrix[1][0:3] )*self.speedFactor      
            self.camMovUp = 0
       
        v = self.camPos       
        glTranslatef(-v[0],-v[1],v[2])      
    
        