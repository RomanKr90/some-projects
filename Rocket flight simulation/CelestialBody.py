'''
Created on 22.04.2013

'''

class CelestialBody(object):
   

    def __init__(self, name, mass, radius, texture = 'blank.jpg', prevpos =None,prevspeed =None, position=[], speed=[],rotPeriod=1, tilt= 0,curRot=0,glRadius =1, satelites = [], show=True, color = []):
        '''
        Constructor
        '''
        self.name = name
        self.mass = mass
        self.radius = radius
        self.texture = texture       
        self.position = position
        self.speed = speed
        self.color = color
        self.rotPeriod = rotPeriod
        self.tilt = tilt
        self.curRot = curRot
        self.glRadius = glRadius
        self.satelites = satelites
        self.show = show
        
        
        # Fuer das Einlesen aller Texturen in den RAM
        from PIL import Image as im
        image = im.open("./images/"+ self.texture)
        self.ix = image.size[0]
        self.iy = image.size[1]
        self.pixels = image.tostring("raw", "RGBX", 0, -1)
        self.radius = radius
        self.angle = 0
        self.texid = None
        self.listid = None
        self.transparent = False
        self.color = [1, 1, 1, 1]
        self.inside = False


        
