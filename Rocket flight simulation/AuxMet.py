'''
Created on 01.07.2013 Roman

'''

import json
from CelestialBody import CelestialBody
import math as m

import os
import numpy as np
import scipy
from scipy.integrate import odeint
from numpy import linalg as LA

ra2deg = 180/ np.pi
#Auxiliary methods

    
     
#Each Horizons File has a signal Line $$SOE from which on forth the needed values are found
#This method is designed to read the initial values for the Odes

def loadplanetsparameter(filename):
    retval = os.getcwd()
    #print "Current working directory %s" % retval
    #os.chdir("/home/roman/Programmes/Eclipse/Mis1/Datafiles")    #Changes directory to the directory with files should be
    
    f = open("Datafiles/" + filename+".txt", "r")    
    while f.readline().strip() != "$$SOE":          #removes Whitespaces for proper comparison and stops at next line after $$SOE
        pass
    f.readline()                   #The second line after $$SOE contains Values                                            
    w = []
    for n in range(0,2):  
        w.append(f.readline().split() )                 #Reads the position and velocity vectors
    
    a = [float(numeric_string) for numeric_string in w[0][0:3]]    #Casts String into numeric values
    b = [float(numeric_string) for numeric_string in w[1][0:3]]  
    
    c = a + b
    
    f.close()
    #os.chdir("/home/roman/Programmes/Eclipse/Mis1")
    return c





#Computes the angles of rotation of the rocketmodel to the right spot from the existing speed vector of the rocket
#In the visualization the rocket is first rotated to the right spot in the xz-Plane (rotated around y axis with the xz-angle)
#and then ascended descended with yz-angle
def computeAngle(v):
   
    yzAngle = compute2DAngleX(v)
    xzAngle = compute2DAngleY([v[0],v[2]])
   
    return [yzAngle,xzAngle]

#Rocketstartposition is at [1,0,0]-[0,0,0]

def compute2DAngleX(v):
    x , y , z = v[0],v[1],v[2]
    cosphi = np.dot([x,0,z],v)/ (LA.norm(v) *LA.norm([x,0,z])  )
    resphi = np.arccos(cosphi)
    #convert in degrees
    resphi = resphi*ra2deg
    if(y>0):
        return resphi*-1 
    #Lower part of the circle
    else:
        return resphi 
#Input x,z

def compute2DAngleY(v):
    #compute angle
    cosphi = np.dot([0,1],v)/LA.norm(v)
    resphi = np.arccos(cosphi)    
    #convert in degrees
    resphi = resphi*ra2deg
    z = v[1]
    x = v[0]
    if(x<0):
        return resphi*-1 
    #Lower part of the circle
    elif(x>0):
        return resphi 
    #If the part of the plane is not certain
    elif(z>0):
        return 0
    elif(z<0):
        return 180
    

#Normalizes a vector
def normalize(v):
    return v/LA.norm(v)
#===============================================================================
# Reads out the Json file and returns the Planet objects
#===============================================================================
def parseJsonFile(filepath ): 
    ''' Daten aus der JSON Datei, oder einer uebergebenen Datei, einlesen '''
    
    jsonFile = open(filepath, "r")  
    data = json.load(jsonFile)  
    jsonFile.close()  
    
    return data,len(data)
    
      

def createCelBody(dictionary, number):
    ''' Erlaubt es CelestialBody Objecte aus den Daten der JSON Datei zu erstellen '''
    return CelestialBody(
                            name = dictionary[number]["name"],
                            mass = dictionary[number]["masse"],
                            radius = dictionary[number]["radius"],
                            texture = dictionary[number]["textur"],
                            position = dictionary[number]["position"],
                            speed = dictionary[number]["speed"],
                            rotPeriod = dictionary[number]["rotation period"],
                            tilt = dictionary[number]["axial tilt"],
                            satelites = []
                        )    
    
#===============================================================================
# reads the Json file with the selected filepath
# index is converted to string as it is in Json file
#===============================================================================
def makeCelBodiesList(filepath):   
    dictionary, numObjects = parseJsonFile(filepath ) 
    L = []
    
    for i in range(0,numObjects):
        L.append(createCelBody(dictionary,str(i)) )          
    return L

def readPlanetsMass(planetsList):
    #Number of planets without the sun
    nPlanets = len(planetsList)-1
    m = np.empty(nPlanets) 
    for i in range(0, nPlanets):
        m[i] = planetsList[i+1].mass            
    return m    
#===============================================================================
# Compute the rotation accomplished by a planet with rotPeriod in days time
#===============================================================================
def computeRotation(days,rotPeriod):
    return days/rotPeriod*360

if __name__ == '__main__':
#===============================================================================
#     a1 = [ 0,   0,1]
#     a2 = [  3 , 1,1]
#     res1 = compute2DAngleY(a1)
#     res2 = compute2DAngleX(a2)
# 
#     print(res1)
#     print(res2)
#===============================================================================
    
    Json,length = parseJsonFile("solarsystem.json")
    print(Json["0"])
    print(length)
    test = makeCelBodiesList("solarsystem.json")
    print(readPlanetsMass(test))
    print(test[0].tilt)