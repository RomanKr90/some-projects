

import OpenGL
OpenGL.ERROR_CHECKING = False

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import CelestialBody
import Simulation

import sys


from OpenGL.raw.GLUT.constants import GLUT_KEY_RIGHT
import numpy as np
from OpenGL.GL.exceptional import glVertex, glColor



class DrawCelBodies(object):

    def __init__(self, sim):
       
        self.sim = sim   
        
    def InitGL(self):
        ''' Initialisierung einiger Einstellungen '''
        glClearColor(0.0, 0.0, 0.0, 1.0)  # Hintergrundfarbe: Schwarz
        glClearDepth(1.0)  # Tiefen Buffer leeren anschalten
        glDepthFunc(GL_LESS)  # Die Art des Tiefentests
        # Turn on depth test, so the later added objects are drawed behind the previous objects, if they have the coordinates behind this objects
        # else new objects are drawn always at the front
        glEnable(GL_DEPTH_TEST)   
        #Turn on the lights 
        glEnable(GL_LIGHTING)      
        #enable LIGHT0, our diffuse Light, which makes the dark side of the rocket brighter
        glEnable (GL_LIGHT0)
        #enable LIGHT1, our diffuse Light, pointing from the sun
        glEnable (GL_LIGHT1)
        glShadeModel(GL_SMOOTH)  # Weiche Farbuebergaenge anschalten   
        #Set the brightness of the highlighting of the opposite side of the rocket
        DiffuseLight0 = [0.1, 0.1, 0.1]     
       
        #Set the color and the brightness of the light of the sun
        DiffuseLight1 = [1, 1, 1]
        #set DiffuseLight to the specified values
        glLightfv (GL_LIGHT0, GL_DIFFUSE, DiffuseLight0)
        glLightfv (GL_LIGHT1, GL_DIFFUSE, DiffuseLight1)      
        
        
        
    
    @staticmethod
    def celestialObject( name, texturePic, size,tilt,rotation  ):
        
        
        glEnable(GL_COLOR_MATERIAL)
        from PIL import Image as coimage
        Texture = coimage.open(texturePic)
        ix = Texture.size[0]
        iy = Texture.size[1]
        Texture = Texture.tostring("raw", "RGBX", 0, -1)
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 1, GL_RGBA, GL_UNSIGNED_BYTE, Texture)
        #glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)
        name = gluNewQuadric()
        gluQuadricNormals(name, GLU_SMOOTH)
        gluQuadricOrientation(name, GLU_OUTSIDE)
        glEnable(GL_TEXTURE_2D)
        gluQuadricTexture(name, GL_TRUE)
        
        glPushMatrix()  
        #First changes the coordinate system to the one with the proper tilt
        glRotatef(tilt,0,0,1)
        #rotate around the y-axis,simulates the rotation of the planet
        glRotatef(rotation,0,1,0)        
        #Sets the texture to the right position by rotating the sphere    
        glRotatef(270,1,0,0)
             
        gluSphere(name, size, 40, 40)    
        glPopMatrix()
        
        glDisable(GL_TEXTURE_2D); 
        gluDeleteQuadric(name)
    
    
  